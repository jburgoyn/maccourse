//
//  AppDelegate.swift
//  WeatherBarAlpha
//
//  Created by Jonny B on 3/20/17.
//  Copyright © 2017 Jonny B. All rights reserved.
//

import Cocoa
import CoreLocation
import Alamofire

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate, CLLocationManagerDelegate {

    // Variables
    let statusItem = NSStatusBar.system().statusItem(withLength: NSVariableStatusItemLength)
    let locationManager = CLLocationManager()
    var currentLocation: CLLocation!
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startMonitoringSignificantLocationChanges()
        locationManager.distanceFilter = 1000
        locationManager.startUpdatingLocation()
        
        statusItem.button?.title = "--°"
        statusItem.action = #selector(AppDelegate.displayPopUp(_:))
        
        let updateWeatherData = Timer.scheduledTimer(timeInterval: 60 * 10, target: self, selector: #selector(downloadWeatherData), userInfo: nil, repeats: true)
        updateWeatherData.tolerance = 60
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocation = locations[locations.count-1]
        Location.sharedInstance.latitude = currentLocation.coordinate.latitude
        Location.sharedInstance.longitude = currentLocation.coordinate.longitude
        downloadWeatherData()
    }
    
    func downloadWeatherData() {
        WeatherService.instance.downloadWeatherDetails(completed: {
            let weather = WeatherService.instance.currentWeather
            self.statusItem.title = "\(weather.currentTemp)°"
            WeatherService.instance.downloadForecast(completed: {
                NotificationCenter.default.post(name: NOTIF_DATA_DOWNLOADED, object: nil)
                self.locationManager.stopUpdatingLocation()
            })
        })
    }

    func displayPopUp(_ sender: AnyObject?) {
        let storyBoard = NSStoryboard(name: "Main", bundle: nil)
        guard let vc = storyBoard.instantiateController(withIdentifier: "ViewController") as? ViewController else { return }
        let popoverView = NSPopover()
        popoverView.contentViewController = vc
        popoverView.behavior = .transient
        popoverView.show(relativeTo: statusItem.button!.bounds, of: statusItem.button!, preferredEdge: .maxY)
        popoverView.behavior = .transient
    }
}

