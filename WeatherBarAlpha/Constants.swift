//
//  Constants.swift
//  WeatherBar
//
//  Created by JonnyB on 3/20/7.
//  Copyright © 2017 JonnyB. All rights reserved.
//

import Foundation

typealias DownloadComplete = () -> ()

let CURRENT_WEATHER_URL = "http://api.openweathermap.org/data/2.5/weather?lat=\(Location.sharedInstance.latitude)&lon=\(Location.sharedInstance.longitude)&units=imperial&appid=42a1771a0b787bf12e734ada0cfc80cb"
let FORECAST_URL = "http://api.openweathermap.org/data/2.5/forecast/daily?lat=\(Location.sharedInstance.latitude)&lon=\(Location.sharedInstance.longitude)&cnt=10&mode=json&units=imperial&appid=42a1771a0b787bf12e734ada0cfc80cb"

let NOTIF_DATA_DOWNLOADED = Notification.Name("dataDownloaded")
let WEATHER_API_URL = "http://openweathermap.org"
