//
//  CurrentWeather.swift
//  WeatherBar
//
//  Created by JonnyB on 3/20/7.
//  Copyright © 2017 JonnyB. All rights reserved.
//

import Cocoa
import Alamofire
import SwiftyJSON

class CurrentWeather {
    fileprivate var _cityName: String!
    fileprivate var _date: String!
    fileprivate var _weatherType: String!
    fileprivate var _currentTemp: Int!
    
    var cityName: String {
        get {
            return _cityName
        } set {
            _cityName = newValue
        }
    }
    
    var weatherType: String {
        get {
            return _weatherType
        } set {
            _weatherType = newValue
        }
    }
    
    var currentTemp: Int {
        get {
            return _currentTemp
        } set {
            _currentTemp = newValue
        }
    }

    var date: String {
        get {
            return _date
        } set {
            _date = newValue
        }
    }
    
    // adding 'class' in front of the function makes this a 'type method'. this means the function is called on the 'type' and not an 'instance' of the type.
    class func loadCurrentWeatherFromData(_ APIData: Data) -> CurrentWeather? {
        let currentWeather = CurrentWeather()
        let json = JSON(data: APIData)

        currentWeather.cityName = json["name"].stringValue.capitalized
        currentWeather.weatherType = json["weather"][0]["main"].stringValue.capitalized
        currentWeather.currentTemp = json["main"]["temp"].intValue
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .none
        let currentDate = dateFormatter.string(from: Date())
        currentWeather.date = "Today, \(currentDate)"
        
        return currentWeather
    }
}













