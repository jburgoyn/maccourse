//
//  Forecast.swift
//  WeatherBar
//
//  Created by JonnyB on 3/20/7.
//  Copyright © 2017 JonnyB. All rights reserved.
//

import Cocoa
import Alamofire
import SwiftyJSON

class Forecast {
    
    fileprivate var _date: String!
    fileprivate var _weatherType: String!
    fileprivate var _highTemp: Int!
    fileprivate var _lowTemp: Int!

    var date: String {
        get {
            return _date
        } set {
            _date = newValue
        }
    }

    var weatherType: String {
        get {
            return _weatherType
        } set {
            _weatherType = newValue
        }
    }
    
    var highTemp: Int {
        get {
            return _highTemp
        } set {
            _highTemp = newValue
        }
    }
    
    var lowTemp: Int {
        get {
            return _lowTemp
        } set {
            _lowTemp = newValue
        }
    }
    
    // adding 'class' in front of the function makes this a 'type method'. this means the function is called on the 'type' and not an 'instance' of the type.
    class func loadForecastFromData(_ APIData: Data) -> [Forecast] {
        var forecast = [Forecast]()
        let json = JSON(data: APIData)
        
        if let list = json["list"].array {
            for day in list {
                let dayForecast = Forecast()
                dayForecast.lowTemp = day["temp"]["min"].intValue
                dayForecast.highTemp = day["temp"]["max"].intValue
                dayForecast.weatherType = day["weather"][0]["main"].stringValue
                
                let date = day["dt"].doubleValue
                let unixConvertedDate = Date(timeIntervalSince1970: date)
                let dateFormatter = DateFormatter()
                dateFormatter.dateStyle = .full
                dateFormatter.dateFormat = "EEEE"
                dateFormatter.timeStyle = .none
                dayForecast._date = unixConvertedDate.dayOfTheWeek()
                
                forecast.append(dayForecast)
            }
        }
        return forecast
    }
}
    
extension Date {
    func dayOfTheWeek() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self)
    }
}













