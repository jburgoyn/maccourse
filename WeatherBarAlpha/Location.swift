//
//  Location.swift
//  WeatherBar
//
//  Created by JonnyB on 3/20/7.
//  Copyright © 2017 JonnyB. All rights reserved.
//

import CoreLocation

class Location {
    static var sharedInstance = Location()
    
    fileprivate var _latitude: Double!
    fileprivate var _longitude: Double!
    
    var latitude: Double {
        get {
            return _latitude
        } set {
            _latitude = newValue
        }
    }
    
    var longitude: Double {
        get {
            return _longitude
        } set {
            _longitude = newValue
        }
    }
}
