//
//  ViewController.swift
//  WeatherBarAlpha
//
//  Created by Jonny B on 3/20/17.
//  Copyright © 2017 Jonny B. All rights reserved.
//

import Cocoa
import CoreLocation
import Alamofire

class ViewController: NSViewController, CLLocationManagerDelegate {

    // Outlets
    @IBOutlet weak var dateLabel: NSTextField!
    @IBOutlet weak var currentTemp: NSTextField!
    @IBOutlet weak var locationLabel: NSTextField!
    @IBOutlet weak var currentWeatherImage: NSImageView!
    @IBOutlet weak var weatherCondition: NSTextField!
    @IBOutlet weak var collectionView: NSCollectionView!
    @IBOutlet weak var poweredByBtn: NSButton!
    @IBOutlet weak var quitBtn: NSButton!
    
    // Variables
    let locationManager = CLLocationManager()
    var currentLocation: CLLocation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    override func viewDidAppear() {
        updateMainUI()
        quitBtn.styleButtonText(button: quitBtn, buttonName: "Quit", fontColor: .darkGray, alignment: .center, font: "Avenir Next", size: 11)
        poweredByBtn.styleButtonText(button: poweredByBtn, buttonName: "Powered by OpenWeatherMap", fontColor: .darkGray, alignment: .center, font: "Avenir Next", size: 11)
        self.view.layer?.backgroundColor = CGColor(red:0.29, green:0.72, blue:0.98, alpha:1.00)
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.dataDownloadedNotif(_:)), name: NOTIF_DATA_DOWNLOADED, object: nil)
    }
    
    override func viewDidDisappear() {
        NotificationCenter.default.removeObserver(self, name: NOTIF_DATA_DOWNLOADED, object: nil)
    }

    @IBAction func poweredByBtnClicked(_ sender: Any) {
        let url = URL(string:WEATHER_API_URL)!
        NSWorkspace.shared().open([url],
                                withAppBundleIdentifier:nil,
                                  additionalEventParamDescriptor:nil,
                                  launchIdentifiers:nil)
    }
    
    @IBAction func quitClicked(_ sender: Any) {
        NSApplication.shared().terminate(nil)
    }
    
    func updateMainUI() {
        let weather = WeatherService.instance.currentWeather
        dateLabel.stringValue = weather.date
        currentTemp.stringValue = "\(weather.currentTemp)°"
        weatherCondition.stringValue = weather.weatherType
        locationLabel.stringValue = weather.cityName
        currentWeatherImage.image = NSImage(named: weather.weatherType)
        collectionView.reloadData()
    }
        
    func dataDownloadedNotif(_ notif: NSNotification) {
        updateMainUI()
    }
}

extension ViewController: NSCollectionViewDelegate, NSCollectionViewDataSource, NSCollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
            let forecastItem = collectionView.makeItem(withIdentifier: "WeatherCell",
                                                     for: indexPath)
            guard let forecastCell = forecastItem as? WeatherCell else { return forecastItem }
            forecastCell.configureCell(weatherCell: WeatherService.instance.forecast[indexPath.item])
            return forecastCell
    }
    
    func collectionView(_ collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
       return WeatherService.instance.forecast.count
    }
    
    func numberOfSections(in collectionView: NSCollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: NSCollectionView, layout collectionViewLayout: NSCollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> NSSize {
        return NSSize(width: 125, height: 125)
    }
}

