//
//  WeatherCell.swift
//  WeatherBarAlpha
//
//  Created by Jonny B on 3/21/17.
//  Copyright © 2017 Jonny B. All rights reserved.
//

import Cocoa

class WeatherCell: NSCollectionViewItem {

    //Outlets
    @IBOutlet weak var dayLbl: NSTextField!
    @IBOutlet weak var lowTempLbl: NSTextField!
    @IBOutlet weak var highTempLbl: NSTextField!
    @IBOutlet weak var weatherImage: NSImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.wantsLayer = true
        self.view.layer?.backgroundColor = CGColor(red:0.69, green:0.85, blue:0.99, alpha:0.5)
        self.view.layer?.cornerRadius = 5
    }
    
    func configureCell(weatherCell: Forecast) {
        dayLbl.stringValue = weatherCell.date
        lowTempLbl.stringValue = "\(weatherCell.lowTemp)°"
        highTempLbl.stringValue = "\(weatherCell.highTemp)°"
        weatherImage.image = NSImage(named: weatherCell.weatherType)
    }
}
