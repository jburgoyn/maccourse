//
//  WeatherService.swift
//  WeatherBarAlpha
//
//  Created by Jonny B on 3/20/17.
//  Copyright © 2017 Jonny B. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class WeatherService {

    static let instance = WeatherService()
    fileprivate var _currentWeather = CurrentWeather()
    fileprivate var _forecast = [Forecast]()
    
    var currentWeather: CurrentWeather {
        get {
            return _currentWeather
        } set {
            _currentWeather = newValue
        }
    }
    
    var forecast: [Forecast] {
        get {
            return _forecast
        } set {
            _forecast = newValue
        }
    }
    
    func downloadWeatherDetails(completed: @escaping DownloadComplete) {
        Alamofire.request(CURRENT_WEATHER_URL).responseData { response in
            self._currentWeather = CurrentWeather.loadCurrentWeatherFromData(response.data!)!
             completed()
        }
    }
    
    func downloadForecast(completed: @escaping DownloadComplete) {
        Alamofire.request(FORECAST_URL).responseData { response in
            self._forecast = Forecast.loadForecastFromData(response.data!)
            if self._forecast.count > 0 {
                self._forecast.remove(at: 0)
                completed()
            }
        }
    }
}
